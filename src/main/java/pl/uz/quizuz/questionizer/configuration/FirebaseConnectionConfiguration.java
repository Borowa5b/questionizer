package pl.uz.quizuz.questionizer.configuration;

import lombok.extern.slf4j.Slf4j;
import net.thegreshams.firebase4j.error.FirebaseException;
import net.thegreshams.firebase4j.service.Firebase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
class FirebaseConnectionConfiguration {

    @Bean
    public Firebase firebase() {
        try {
            return new Firebase("https://quizuz.firebaseio.com");
        } catch (FirebaseException e) {
            log.warn("Couldn't get firebase connection: {0}", e);
        }
        return null;
    }
}
