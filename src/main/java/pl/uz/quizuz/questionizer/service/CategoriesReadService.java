package pl.uz.quizuz.questionizer.service;

import lombok.extern.slf4j.Slf4j;
import net.thegreshams.firebase4j.error.FirebaseException;
import net.thegreshams.firebase4j.model.FirebaseResponse;
import net.thegreshams.firebase4j.service.Firebase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Slf4j
@Service
public class CategoriesReadService {

    private final Firebase firebase;

    @Autowired
    public CategoriesReadService(Firebase firebase) {
        this.firebase = firebase;
    }

    public String getCategories() {
        try {
            final FirebaseResponse firebaseResponse = firebase.get("categories");
            return firebaseResponse.getRawBody();
        } catch (FirebaseException | UnsupportedEncodingException e) {
            log.warn("There was problem fetching categories: {0}", e);
        }
        return null;
    }
}
